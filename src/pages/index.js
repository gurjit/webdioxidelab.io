import React from "react";
import Link from "gatsby-link";

import { rhythm } from "../utils/typography";

import FeaturedPost from "../components/FeaturedPost.jsx";
import AllPosts from "../components/AllPosts.jsx";

const IndexPage = ({ data }) => {
  return (
    <div className="wrapper">
      <FeaturedPost
        postData = {data.allMarkdownRemark.edges[0]}
      />
      <AllPosts
         posts = {data.allMarkdownRemark.edges.slice(1)}
         featured
      />
      </div>
  );
};

export default IndexPage;

export const query = graphql`
  query IndexPageQuery {
    allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            image
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`;