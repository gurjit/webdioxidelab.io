import React from "react";
import Link from "gatsby-link";

const Button = (props) => {
  return (
    <Link
            to={props.postLink}
            css={{ textDecoration: `none`, color: `inherit` }}
          >
    <button className="solid-btn">Read More ...</button>
    </Link>
  )
}

export default Button;