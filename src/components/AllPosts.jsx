import React from "react";
import Link from "gatsby-link";

import Button from "../components/Button.jsx";

const AllPosts = (props) => {
  return (
    <div className={`all-posts ${props.featured ? 'featured' : ''}`}>
      <h3 className="sidebar-heading"> More Posts </h3>
      {props.posts.map(({ node }) => (
        <div className="post" key={node.id}>
        <Link
            to={node.fields.slug}
            css={{ textDecoration: `none`, color: `inherit` }}
          >
          <div>
          <p>
            {node.frontmatter.title}{" "}
          </p>
          <span>— {node.frontmatter.date}</span>
          </div>
        </Link>
        </div>
      ))}
    </div>
  )
}

export default AllPosts;