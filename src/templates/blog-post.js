import React from "react";
import Helmet from 'react-helmet';

import AllPosts from "../components/AllPosts.jsx";


const BlogPost = ({ data }) => {
  const post = data.markdownRemark;
  return (
    <main className="post-page-wrapper">
      <Helmet
        title= {post.frontmatter.title}
        meta={[
          { name: 'description', content: post.frontmatter.date + " " + post.excerpt + " Read more" },
          { name: 'keywords', content: post.frontmatter.keywords },
          { name: 'author', content: post.frontmatter.author },
        ]}
      />
      <section className="post-wrapper">
        <div className="post-content">
          <h1 className="post-title">{post.frontmatter.title}</h1>
            <div className="post-toc mobile-only" dangerouslySetInnerHTML={{__html: data.markdownRemark.tableOfContents}} /> 
            <div dangerouslySetInnerHTML={{ __html: post.html}} />
        </div>
      </section>
      <section className="sidebar table-of-contents">
        <h3 className="sidebar-heading"> Table Of Contents </h3>
        <div dangerouslySetInnerHTML={{__html: data.markdownRemark.tableOfContents}} />
      </section>
    </main>
  );
};

export default BlogPost;

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        date
        keywords
        author
      }
      timeToRead
      tableOfContents
      headings {
        value
        depth
      }
      excerpt
    }
  }
`;