import Typography from "typography";
import lincolnTheme from "typography-theme-lincoln";
import doelgerTheme from "typography-theme-doelger";
import grandViewTheme from "typography-theme-grand-view";

lincolnTheme.googleFonts.push({
  name:'Crimson Text',
  styles: ['400','400i','600','600i','700','700i']
});

console.log(lincolnTheme.googleFonts);

lincolnTheme.bodyFontFamily = ["Crimson Text"];

const typography = new Typography(grandViewTheme);

export default typography;